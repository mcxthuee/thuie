//============================================================================
// Name        : fileTree.cpp
// Author      : Donghui Li
//============================================================================

#include <iostream>
#include <fstream>
#include <string>
#include <stack>

using namespace std;

class Tree {
public:
	Tree() :
			root(0) {
	}

	~Tree() {
		remove(root);
	}

	void build(istream & in) {
		string filename;

		in >> filename;
		root = new Node(filename);
		readfolder(in, root);
	}

	//listAll without recursive call
	void listAll(ostream &out) {
		stack<Node*> s;

		//Start form root at depth 0
		Node* current = root;
		int depth = 0;

		do {
			for (int i = 0; i < depth; ++i)
				out << "    ";
			out << current->filename << endl;

			//Is folder?
			if (current->firstChild) {
				s.push(current);
				++depth;
				current = current->firstChild;
			} else if (current->nextSibling) {
				current = current->nextSibling;
			} else {
				//Finish printing folder, find next non-null sibling
				do {
					current = s.top();
					s.pop();
					--depth;
				} while (!current->nextSibling && current != root);

				current = current->nextSibling;
			}
		} while (current);
	}
private:
	struct Node {
		string filename;
		Node *firstChild;
		Node *nextSibling;

		Node(const string &fn = string(), Node *fc = 0, Node *ns = 0) :
				filename(fn), firstChild(fc), nextSibling(ns) {
		}
	};

	//listAll by recursion
	void listAll(Node *n, int depth = 0) {
		if (n) {
			for (int i = 0; i < depth; ++i)
				cout << "    ";
			cout << n->filename << endl;
			listAll(n->firstChild, depth + 1);
			listAll(n->nextSibling, depth);
		}
	}

	void readfolder(istream &in, Node *n) {
		string filename;

		in >> filename;
		if (filename == "}")
			return;

		//Is folder?
		if (filename == "{") {
			in >> filename;
			n->firstChild = new Node(filename);
			readfolder(in, n->firstChild);

			//Try to get its sibling
			in >> filename;
			if (filename == "}" || in.fail())
				return;
		}

		//If haven't return, add sibling
		n->nextSibling = new Node(filename);
		readfolder(in, n->nextSibling);
	}

	void remove(Node * n) {
		if (n) {
			remove(n->firstChild);
			remove(n->nextSibling);
			delete n;
		}
	}

	Node *root;
};

int main() {
	ifstream fin("in.txt");
	ofstream fout("out.txt");

	Tree t;

	t.build(fin);

	t.listAll(fout);
}
