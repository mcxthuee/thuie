//============================================================================
// Name        : tranFix.cpp
// Author      : Donghui Li
//============================================================================

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stack>
#include <map>
#include <cctype>
#include <stdexcept>

using namespace std;

class in2post {
public:
	in2post(const string & s = string()) {
		//保存各个算符的优先级
		pr['+'] = 1;
		pr['-'] = 1;
		pr['*'] = 3;
		pr['/'] = 3;
		pr['('] = 5;

		exp = s;
	}

	string postfix() {
		istringstream in(exp);
		string ret;
		char c;
		while (in) {
			if (isalpha(in.peek())) {
				in >> c;
				ret += c;
			} else {
				in >> c;
				if (in.fail())
					break;
				if (c == ')') {
					while (!op.empty() && op.top() != '(') {
						popOp(ret);
					}
					op.pop();
				} else {
					while (!op.empty() && op.top() != '('
							&& pr[c] <= pr[op.top()]) {
						popOp(ret);
					}
					op.push(c);
				}
			}
		}
		while (!op.empty())
			popOp(ret);
		return ret;
	}
private:
	void popOp(string &s) {
		char c = op.top();
		op.pop();
		s += c;
	}
	stack<char> op;
	map<char, int> pr;
	string exp;
};

class post2in {
public:
	post2in(const string & p = string()) {
		postfix = p;
	}

	string infix() {
		infix(postfix.length(), 0);
		return ret;
	}
private:
	string postfix;
	string ret;

	int infix(size_t end, char outerOp) {
		if (isalpha(postfix[end - 1])) {
			ret = postfix[end - 1] + ret;
			return end - 1;
		} else {
			char op = postfix[end - 1];

			bool bracket = false;
			if ((outerOp == '*' || outerOp == '/') && (op == '+' || op == '-'))
				bracket = true;

			if (bracket)
				ret = ')' + ret;

			int mid = infix(end - 1, op);

			ret = op + ret;

			int beg = infix(mid, op);

			if (bracket)
				ret = '(' + ret;

			return beg;
		}
	}
};

int main() {
	ifstream fin("in.txt");
	ofstream fout("out.txt");
	string line;

	getline(fin, line);
	fout << in2post(line).postfix() << endl;

	getline(fin, line);
	fout << post2in(line).infix() << endl;

	return 0;
}
