//============================================================================
// Name        : closestPair.cpp
// Author      : Donghui Li
//============================================================================

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

struct Point {
	Point(int xx = 0, int yy = 0) :
			x(xx), y(yy) {
	}
	int x, y;
};

ostream &operator<<(ostream & out, const Point & p) {
	out << '(' << p.x << ", " << p.y << ')';
	return out;
}

inline double sqr(int x) {
	return x * x;
}

double dist(Point*a, Point*b) {
	return sqrt(sqr(a->x - b->x) + sqr(a->y - b->y));
}

struct Pair {
	Pair(Point*aa = 0, Point*bb = 0) :
			a(aa), b(bb) {
		if (a && b)
			d = dist(a, b);
		else
			d = -1;
	}
	Point *a, *b;
	double d;
};

ostream &operator<<(ostream & out, const Pair & p) {
	out << *p.a << ' ' << *p.b;
	return out;
}

bool compX(Point*a, Point*b) {
	return a->x < b->x;
}

bool compY(Point*a, Point*b) {
	return a->y < b->y;
}

Pair closestPair(vector<Point*> &p, size_t beg, size_t end) {
	Pair close;
	//using brute-force for small set
	if (end - beg < 4) {
		size_t i, j;

		for (i = beg; i < end; ++i)
			for (j = i + 1; j < end; ++j)
				if (close.d < 0 || dist(p[i], p[j]) < close.d)
					close = Pair(p[i], p[j]);

		sort(p.begin() + beg, p.begin() + end, compY);
	} else {
		int mid = (beg + end) / 2;
		int xm = p[mid]->x;

		Pair pairL, pairR;

		pairL = closestPair(p, beg, mid);
		pairR = closestPair(p, mid, end);

		if (pairL.d < pairR.d)
			close = pairL;
		else
			close = pairR;

		inplace_merge(p.begin() + beg, p.begin() + mid, p.begin() + end, compY);

		vector<Point*> strip;
		size_t i, j;

		for (i = beg; i < end; ++i)
			if (abs(xm - p[i]->x) < close.d)
				strip.push_back(p[i]);

		for (i = 0; i < strip.size(); ++i)
			for (j = i + 1;
					j < strip.size() && strip[j]->y - strip[i]->y < close.d;
					++j)
				if (dist(strip[i], strip[j]) < close.d)
					close = Pair(strip[i], strip[j]);
	}
	if(close.d<0)
		cerr<<beg<<' '<<end<<endl;
	return close;
}

Pair closestPair(vector<Point*> &p) {
	sort(p.begin(), p.end(), compX);
	return closestPair(p, 0, p.size());
}

int main() {
	vector<Point*> p;

	ifstream fin("in.txt");
	if (!fin) {
		cerr << "Cannot open in.txt for input!" << endl;
		return 1;
	} else {
		int x, y;
		while (fin) {
			fin >> x >> y;
			if (fin.fail())
				break;
			p.push_back(new Point(x, y));
		}
	}

	Pair res = closestPair(p);

	ofstream fout("out.txt");
	fout << res << endl;
	fout.close();

	return 0;
}
