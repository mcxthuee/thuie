#ifndef AVL_TREE_H
#define AVL_TREE_H

#include <iostream>
using namespace std;

template<typename Comparable>
class AVLTree {
public:
	AVLTree() :
			root(NULL) {
	}
	AVLTree(const AVLTree & rhs) :
			root(NULL) {
		*this = rhs;
	}

	~AVLTree() {
		makeEmpty();
	}

	void makeEmpty() {
		makeEmpty(root);
	}

	void insert(const Comparable & x) {
		insert(x, root);
	}

	void insertFromStream(istream & in) {
		Comparable x;
		in >> x;
		while (!in.fail()) {
			insert(x);
			in >> x;
		}
	}

	void listAll(ostream & out) {
		listAll(out, root, 0);
	}
private:
	struct AVLNode {
		Comparable element;
		AVLNode *left;
		AVLNode *right;
		int height;

		AVLNode(const Comparable & e, AVLNode *lt, AVLNode *rt, int h = 0) :
				element(e), left(lt), right(rt), height(h) {
		}
	};

	AVLNode *root;

	void insert(const Comparable & x, AVLNode * & t) {
		if (t == NULL)
			t = new AVLNode(x, NULL, NULL);
		else if (x < t->element) {
			insert(x, t->left);
			if (height(t->left) - height(t->right) == 2) {
				if (x < t->left->element)
					rotateWithLeftChild(t);
				else
					doubleWithLeftChild(t);
			}
		} else if (t->element < x) {
			insert(x, t->right);
			if (height(t->right) - height(t->left) == 2) {
				if (t->right->element < x)
					rotateWithRightChild(t);
				else
					doubleWithRightChild(t);
			}
		} else
			; // Duplicate; do nothing
		t->height = max(height(t->left), height(t->right)) + 1;
	}

	void makeEmpty(AVLNode * & t) {
		if (t != NULL) {
			makeEmpty(t->left);
			makeEmpty(t->right);
			delete t;
		}
		t = NULL;
	}

	int height(AVLNode *t) const {
		return t == NULL ? -1 : t->height;
	}

	/**
	 * Rotate binary tree node with left child.
	 * For AVL trees, this is a single rotation for case 1.
	 * Update heights, then set new root.
	 */
	void rotateWithLeftChild(AVLNode * & k2) {
		AVLNode *k1 = k2->left;
		k2->left = k1->right;
		k1->right = k2;
		k2->height = max(height(k2->left), height(k2->right)) + 1;
		k1->height = max(height(k1->left), k2->height) + 1;
		k2 = k1;
	}

	/**
	 * Rotate binary tree node with right child.
	 * For AVL trees, this is a single rotation for case 4.
	 * Update heights, then set new root.
	 */
	void rotateWithRightChild(AVLNode * & k1) {
		AVLNode *k2 = k1->right;
		k1->right = k2->left;
		k2->left = k1;
		k1->height = max(height(k1->left), height(k1->right)) + 1;
		k2->height = max(height(k2->right), k1->height) + 1;
		k1 = k2;
	}

	/**
	 * Double rotate binary tree node: first left child.
	 * with its right child; then node k3 with new left child.
	 * For AVL trees, this is a double rotation for case 2.
	 * Update heights, then set new root.
	 */
	void doubleWithLeftChild(AVLNode * & k3) {
		rotateWithRightChild(k3->left);
		rotateWithLeftChild(k3);
	}

	/**
	 * Double rotate binary tree node: first right child.
	 * with its left child; then node k1 with new right child.
	 * For AVL trees, this is a double rotation for case 3.
	 * Update heights, then set new root.
	 */
	void doubleWithRightChild(AVLNode * & k1) {
		rotateWithLeftChild(k1->right);
		rotateWithRightChild(k1);
	}

	void listAll(ostream & out, AVLNode *n, int depth = 0) {
		if (n) {
			for (int i = 0; i < depth; ++i)
				out << "  ";
			out << n->element << endl;
			listAll(out, n->left, depth + 1);
			listAll(out, n->right, depth + 1);
		}
	}
};

#endif
