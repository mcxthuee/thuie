//============================================================================
// Name        : cessor.cpp
// Author      : Donghui Li
//============================================================================

#include <iostream>
#include <fstream>

#include "BinarySearchTree.h"

using namespace std;

int main() {
	ifstream fin("in.txt");
	BinarySearchTree bst;
	bst.insertFromStream(fin);
	int x, predecessor, successor;

	cout << "input node value (input 8888 to exit) : ";
	cin >> x;

	while (x != 8888) {
		predecessor = bst.predecessor(x);
		successor = bst.successor(x);
		if (predecessor == NON && successor == NON)
			cout << "this node is not in the tree" << endl;
		else {
			if (predecessor == NON)
				cout << "this node does not have predecessor" << endl;
			else
				cout << "predecessor : " << predecessor << endl;
			if (successor == NON)
				cout << "this node does not have successor" << endl;
			else
				cout << "successor : " << successor << endl;
		}
		cout << "input node value (input 8888 to exit) : ";
		cin >> x;
	}

	cout << "bye" << endl;

	return 0;
}
