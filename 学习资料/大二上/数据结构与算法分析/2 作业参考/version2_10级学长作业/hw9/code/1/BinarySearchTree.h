#ifndef BINARY_SEARCH_TREE_H
#define BINARY_SEARCH_TREE_H

#define MAX  100
#define MIN -100
#define NON -1000

#include <iostream>
using namespace std;

class BinarySearchTree {
public:
	BinarySearchTree() :
			root(NULL) {
	}

	~BinarySearchTree() {
		makeEmpty();
	}

	void makeEmpty() {
		makeEmpty(root);
	}

	void insert(const int & x) {
		insert(x, root);
	}

	void insertFromStream(istream & in) {
		int x;
		in >> x;
		while (!in.fail()) {
			insert(x);
			in >> x;
		}
	}

	int predecessor(int x) {
		return predecessor(x, root, MIN, MAX);
	}

	int successor(int x) {
		return successor(x, root, MIN, MAX);
	}
private:
	struct BinaryNode {
		int element;
		BinaryNode *left;
		BinaryNode *right;

		BinaryNode(const int & e, BinaryNode *lt, BinaryNode *rt) :
				element(e), left(lt), right(rt) {
		}
	};

	BinaryNode *root;

	void insert(const int & x, BinaryNode * & t) {
		if (t == NULL)
			t = new BinaryNode(x, NULL, NULL);
		else if (x < t->element)
			insert(x, t->left);
		else if (t->element < x)
			insert(x, t->right);
		else
			; // Duplicate; do nothing
	}

	/**
	 * Internal method to make subtree empty.
	 */
	void makeEmpty(BinaryNode * & t) {
		if (t != NULL) {
			makeEmpty(t->left);
			makeEmpty(t->right);
			delete t;
		}
		t = NULL;
	}

	int findMin(BinaryNode *n) const {
		if (n == NULL)
			return NON;
		if (n->left == NULL)
			return n->element;
		return findMin(n->left);
	}

	int findMax(BinaryNode *n) const {
		if (n == NULL)
			return NON;
		while (n->right != NULL)
			n = n->right;
		return n->element;
	}

	int predecessor(int x, BinaryNode * n, int min, int max) {
		if (n == NULL)
			return NON;
		else {
			if (x < n->element)
				return predecessor(x, n->left, min, n->element);
			else if (x > n->element)
				return predecessor(x, n->right, n->element, max);
			else {
				if (n->left)
					return findMax(n->left);
				else if (min > MIN)
					return min;
				else
					return NON;
			}
		}
	}

	int successor(int x, BinaryNode * n, int min, int max) {
		if (n == NULL)
			return NON;
		else {
			if (x < n->element)
				return successor(x, n->left, min, n->element);
			else if (x > n->element)
				return successor(x, n->right, n->element, max);
			else {
				if (n->right)
					return findMin(n->right);
				else if (max < MAX)
					return max;
				else
					return NON;
			}
		}
	}
};
#endif
