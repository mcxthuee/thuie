#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "quickSort.h"
#include "dtime.h"

using namespace std;

template<typename T>
ostream & operator<<(ostream & out, const vector<T> & v) {
	for (typename vector<T>::const_iterator i = v.begin(); i != v.end(); ++i)
		out << *i << ' ';

	return out;
}

template<typename T>
istream & operator>>(istream & in, vector<T> & v) {
	T x;
	in >> x;
	while (!in.fail()) {
		v.push_back(x);
		in >> x;
	}

	return in;
}

int main() {
	int n;
	vector<int> v;

	ifstream fin("in.txt");
	fin >> v;
	n=v.size();


	dTime();
	quickSort(v);
	cout <<n<<"\t"<< dTime() << endl;

	ofstream fout("out.txt");
	fout << v <<endl;
	fout.close();

	return 0;
}