/*
 * quickSort.h
 *
 *  Created on: 2012-3-16
 *      Author: ldh
 */

#ifndef QUICKSORT_H_
#define QUICKSORT_H_

#include <vector>
using namespace std;

template<typename T>
const T & meian3(vector<T>& v, int l, int r) {
	int m = (l + r) / 2;
	if (v[l] > v[m])
		swap(v[l], v[m]);
	if (v[l] > v[r])
		swap(v[l], v[r]);
	if (v[m] > v[r])
		swap(v[m], v[r]);

	swap(v[m], v[r - 1]);
	return v[r - 1];
}

template<typename T>
void insertionSort(vector<T> & v, int l, int r) {
	int i, j, t;
	for (i = l + 1; i <= r; ++i) {
		t = v[i];
		for (j = i; j > 0 && t < v[j - 1]; --j)
			v[j] = v[j - 1];
		v[j] = t;
	}
}

template<typename T>
void quickSort(vector<T> &v, int l, int r) {
	if (r - l > 30) {
		T p = meian3(v, l, r);
		int i = l, j = r - 1;
		for (;;) {
			while (v[++i] < p)
				;
			while (v[--j] > p)
				;
			if (i < j)
				swap(v[i], v[j]);
			else
				break;
		}
		swap(v[i], v[r - 1]);

		quickSort(v, l, i - 1);
		quickSort(v, i + 1, r);
	} else
		insertionSort(v, l, r);
}

template<typename T>
void quickSort(vector<T> &v) {
	quickSort(v, 0, v.size()-1);
}

#endif /* QUICKSORT_H_ */
