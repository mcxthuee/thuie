/*
 * dtime.h
 *
 *  Created on: 2012-3-16
 *      Author: ldh
 */

#ifndef DTIME_H_
#define DTIME_H_

#include <ctime>

using namespace std;

//This function will return the time interval of this call and last call of dTime
double dTime() {
	static clock_t last, now;
	static double dt;

	now=clock();

	dt = (double) (now - last) / CLOCKS_PER_SEC;

	last=clock();

	return dt;
}

#endif /* DTIME_H_ */
