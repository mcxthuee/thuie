/*
 * hanoi.cpp
 *
 *  Created on: 2012-3-9
 *      Author: ldh
 */

#include <iostream>
#include <fstream>

using namespace std;

class Hanoi {
public:
	Hanoi(ostream & o = cout) :
			out(o) {
	}

	void solve(int n) {
		move(n, 'A', 'B', 'C');
	}
private:
	ostream & out;

	//move n disk(s) form a to c with the help of b
	void move(int n, char a, char b, char c) {
		if (n > 0) {
			move(n - 1, a, c, b);
			out << "把盘子" << n << "从" << a << "拿到" << c << endl;
			move(n - 1, b, a, c);
		}
	}
};

int main() {
	ofstream fout("out.txt");
	if (!fout) {
		cerr << "无法创建out.txt" << endl;
		return 2;
	}

	Hanoi obj(fout);

	obj.solve(10);

	return 0;
}
