//============================================================================
// Name        : maxCircle.cpp
// Author      : 李冬辉
// Description : 一个简单的暴力搜索算法
//============================================================================

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

//为了简化代码，使用了全局变量

vector<vector<int> > graph; //用邻接矩阵保存图
vector<int> maxCircle, currentPath; //最大圈和当前路径
vector<bool> visit; //是否已经到过

int n, m, start; //顶点数、边数、起点

//输入输出文件
ifstream fin("in.txt");
ofstream fout("out.txt");

//递归访问节点x
void walk(int x) {
	currentPath.push_back(x); //保存x到当前路径

	if (start == x) {
		//如果起点和当前点相同，保存最长圈，并且不再深入递归
		if (currentPath.size() > maxCircle.size())
			maxCircle = currentPath;
	} else {
		//由于起点的特殊性，这里进行了一些处理
		if (start < 0)
			start = x;
		else
			visit[x] = true;

		//寻找所有与x相连且没有访问过的节点进行递归
		for (int i = 0; i < n; ++i)
			if (graph[x][i] > 0 && !visit[i]) {
				walk(i);
			}

		visit[x] = false;
	}

	currentPath.pop_back(); //清除x
}

int main() {
	fin >> n >> m;

	//设置vector大小并初始化
	graph.resize(n, vector<int>(n, 0));
	visit.resize(n, false);

	//读入图
	int i;
	char u, v;
	for (i = 0; i < m; ++i) {
		fin >> u >> v;
		//将字母转化为序号
		u -= 'A';
		v -= 'A';
		++graph[u][v];
		++graph[v][u];
	}

	//枚举起点，寻找圈
	for (i = 0; i < n; ++i) {
		start = -1;
		walk(i);
	}

	if (maxCircle.empty())
		fout << "No Circle Found!" << endl;//如果最大圈为空，说明没有找到
	else {
		for (i = 0; i < maxCircle.size(); ++i)
			fout << char('A' + maxCircle[i]);
		fout << endl;
	}

	return 0;
}
