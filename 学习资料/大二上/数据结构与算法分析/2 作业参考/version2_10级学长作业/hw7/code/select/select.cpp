//============================================================================
// Name        : select.cpp
// Author      : Donghui Li
//============================================================================

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

//Selection sort function which will end when finish sorting range [l, k]
template<typename T>
void selectionSort(vector<T> & v, int l, int r, int k = -1) {
	if (k < 0)
		k = r;
	int i, j, min;
	for (i = l; i <= k; ++i) {
		min = i;
		for (j = i + 1; j <= r; ++j)
			if (v[min] > v[j])
				min = j;
		swap(v[i], v[min]);
	}
}

//It will put the median of the 5 elements in v[l+2]
template<typename T>
void median5(vector<T>& v, int l) {
	selectionSort(v, l, l+4, l+2);
}

template<typename T>
void select(vector<T> &v, int l, int r, int k);

//Calculate the median-of-median-of-5 and put it in v[r]
template<typename T>
const T & medianmedian5(vector<T>& v, int l, int r) {
	int i, n;
	n = (r - l + 1) / 5;
	for (i = 0; i < n; ++i) {
		median5(v, l + i * 5);
		swap(v[l + i], v[l + i * 5 + 2]);
	}
	select(v, l, l + n - 1, l + n / 2);
	swap(v[r], v[l + n / 2]);
	return v[r];
}

template<typename T>
void select(vector<T> &v, int l, int r, int k) {
	if (r - l > 30) {
		T p = medianmedian5(v, l, r);
		int i = l, j = r;
		for (;;) {
			while (v[++i] < p)
				;
			while (v[--j] > p)
				;
			if (i < j)
				swap(v[i], v[j]);
			else
				break;
		}
		swap(v[i], v[r]);

		if (i > k)
			select(v, l, i - 1, k);
		else if (i < k)
			select(v, i + 1, r, k);
	} else
		selectionSort(v, l, r, k);//For small range, just use selectionSort
}

//The helper
template<typename T>
T select(vector<T> &v, int k) {
	select(v, 0, v.size()-1, k - 1);
	return v[k-1];
}

//Reload >> for vector
template<typename T>
istream & operator>>(istream & in, vector<T> & v) {
	T x;
	in >> x;
	while (!in.fail()) {
		v.push_back(x);
		in >> x;
	}

	return in;
}

int main() {
	ifstream fin("in.txt");
	vector<int> a;
	fin >> a;

	ofstream fout("out.txt");
	fout << select(a, 19) << endl;
	fout.close();

	return 0;
}
