#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass article
\begin_preamble
% 如果没有这一句命令，XeTeX会出错，原因参见
% http://bbs.ctex.org/viewthread.php?tid=60547
\DeclareRobustCommand\nobreakspace{\leavevmode\nobreak\ }
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package none
\inputencoding auto
\fontencoding global
\font_roman newcent
\font_sans helvet
\font_typewriter courier
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format pdf4
\output_sync 0
\bibtex_command default
\index_command default
\float_placement H
\paperfontsize default
\spacing single
\use_hyperref false
\papersize a4paper
\use_geometry true
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2cm
\topmargin 3cm
\rightmargin 2cm
\bottommargin 3cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Assignment 3 of DOE
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash

\backslash

\end_layout

\end_inset

Reading Comprehension
\end_layout

\begin_layout Author
LIU Zhe, 2009010845
\end_layout

\begin_layout Date
April 23, 2012
\end_layout

\begin_layout Section
Data Description
\end_layout

\begin_layout Standard
First we summarize the data with various statistics, the results are as
 follows:
\end_layout

\begin_layout LyX-Code
Variable  Group   N    Mean  StDev  Minimum     Q1  Median      Q3  Maximum
 
\end_layout

\begin_layout LyX-Code
PRE1      Basal  22  10.500  2.972    4.000  8.000  11.500  12.000   16.000
\end_layout

\begin_layout LyX-Code
          DRTA   22   9.727  2.694    6.000  8.000   9.000  12.000   16.000
\end_layout

\begin_layout LyX-Code
          Strat  22   9.136  3.342    4.000  6.000   8.500  12.250   14.000
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code
PRE2      Basal  22   5.273  2.763    2.000  3.000   5.000   7.000   13.000
\end_layout

\begin_layout LyX-Code
          DRTA   22   5.091  1.998    1.000  4.000   6.000   6.250    8.000
\end_layout

\begin_layout LyX-Code
          Strat  22   4.955  1.864    2.000  3.000   5.000   6.000    9.000
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code
POST1     Basal  22   6.682  2.767    2.000  4.750   6.500   9.000   12.000
\end_layout

\begin_layout LyX-Code
          DRTA   22   9.773  2.724    5.000  7.750  10.000  12.000   14.000
\end_layout

\begin_layout LyX-Code
          Strat  22   7.773  3.927    1.000  4.000   7.000  11.250   15.000
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code
POST2     Basal  22   5.545  2.041    3.000  4.000   5.000   7.250   10.000
\end_layout

\begin_layout LyX-Code
          DRTA   22   6.227  2.092    0.000  6.000   6.000   7.000   11.000
\end_layout

\begin_layout LyX-Code
          Strat  22   8.364  2.904    1.000  6.750   9.000  10.250   13.000
\end_layout

\begin_layout LyX-Code

\end_layout

\begin_layout LyX-Code
POST3     Basal  22   41.05   5.64    32.00  36.00   41.00   45.00    54.00
\end_layout

\begin_layout LyX-Code
          DRTA   22   46.73   7.39    30.00  41.75   48.50   53.00    57.00
\end_layout

\begin_layout LyX-Code
          Strat  22   44.27   5.77    33.00  41.75   45.00   49.00    53.00
\end_layout

\begin_layout Standard
Next we draw Box plots to graphically show the data.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Boxplot.png
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Box plots of the data
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
From the statistics and the above figure, we can get some findings:
\end_layout

\begin_layout Itemize
The Pretests on the first and second reading comprehension measures give
 very close scores for different groups, but the overall performance of
 PRE1 is better than PRE2.
\end_layout

\begin_layout Itemize
There is obvious difference in scores among three groups in all Posttest
 measures.
 In POST1, DRTA has a higher mean score; in POST2, Strat has a higher mean
 score; and in POST3, DRTA has a higher one.
\end_layout

\begin_layout Itemize
POST3 gives much higher scores for all groups than POST1 and POST2.
\end_layout

\begin_layout Section
Analysis of Residuals
\end_layout

\begin_layout Standard
Now we perform residual analysis for each of the pre- and post- tests to
 check the assumptions of the ANOVA model.
\end_layout

\begin_layout Subsection
Residual Analysis for PRE1
\end_layout

\begin_layout Standard
We draw various residual plots for PRE1 using Minitab, the figure below
 shows the results.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Residual_PRE1.png
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Residual plots for PRE1
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
Findings and inferences:
\end_layout

\begin_layout Enumerate
The normal probability plot resembles a straight line very well and the
 histogram resembles the shape of a normal distribution, which confirm the
 normality of the errors.
\end_layout

\begin_layout Enumerate
The plot of residuals versus fitted values shows no obvious pattern, which
 implies that the residuals are structureless.
\end_layout

\begin_layout Enumerate
The plot of residuals versus run order shows no obvious pattern, so there
 is no reason to suspect any violation of the independence assumption.
\end_layout

\begin_layout Subsection
Residual Analysis for PRE2
\end_layout

\begin_layout Standard
The figure below shows various residual plots for PRE2 using Minitab.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Residual_PRE2.png
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Residual plots for PRE2
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Findings and inferences:
\end_layout

\begin_layout Enumerate
The normal probability plot generally resembles a straight line and the
 histogram resembles the shape of a normal distribution, but there seems
 to be one outlier.
 Nonetheless, since this plot is not grossly non-normal, we still think
 the normality assumption is valid.
\end_layout

\begin_layout Enumerate
The plot of residuals versus fitted values shows no obvious pattern, which
 implies that the residuals are structureless.
\end_layout

\begin_layout Enumerate
The plot of residuals versus run order shows no obvious pattern, so there
 is no reason to suspect any violation of the independence assumption.
\end_layout

\begin_layout Subsection
Residual Analysis for POST1
\end_layout

\begin_layout Standard
The figure below shows various residual plots for POST1 using Minitab.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Residual_POST1.png
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Residual plots for POST1
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Findings and inferences:
\end_layout

\begin_layout Enumerate
The normal probability plot resembles a straight line perfectly and the
 histogram resembles the shape of a normal distribution very well, which
 confirm the normality of the errors.
\end_layout

\begin_layout Enumerate
The plot of residuals versus fitted values shows no obvious pattern although
 the range of residual of the second fitted value is a little large than
 the others, so the plot generally implies that the residuals are structureless.
\end_layout

\begin_layout Enumerate
The plot of residuals versus run order shows no obvious pattern, so there
 is no reason to suspect any violation of the independence assumption.
\end_layout

\begin_layout Subsection
Residual Analysis for POST2
\end_layout

\begin_layout Standard
The figure below shows various residual plots for POST2 using Minitab.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Residual_POST2.png
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Residual plots for POST2
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Findings and inferences:
\end_layout

\begin_layout Enumerate
The normal probability plot generally resembles a straight line and the
 histogram resembles the shape of a normal distribution.
 However, the tendency of the normal probability plot bends upward slightly
 on the left side, which implies that the left tail of the error distribution
 is somewhat thicker than would be anticipated in a normal distribution
 This plot is not grossly non normal, however, hence we still think that
 the normality assumption is not violated.
\end_layout

\begin_layout Enumerate
The plot of residuals versus fitted values shows no obvious pattern, which
 implies that the residuals are structureless.
\end_layout

\begin_layout Enumerate
The plot of residuals versus run order shows no obvious pattern, so there
 is no reason to suspect any violation of the independence assumption.
\end_layout

\begin_layout Subsection
Residual Analysis for POST3
\end_layout

\begin_layout Standard
The figure below shows various residual plots for POST3 using Minitab.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename Residual_POST3.png
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Residual plots for POST3
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Findings and inferences:
\end_layout

\begin_layout Enumerate
The normal probability plot resembles a straight line well, and the histogram
 resembles the shape of a normal distribution except that its mode is slightly
 deviated to right of 0.
 These plots confirm the normality of the errors in general, however.
\end_layout

\begin_layout Enumerate
The plot of residuals versus fitted values shows no obvious pattern, which
 implies that the residuals are structureless.
\end_layout

\begin_layout Enumerate
The plot of residuals versus run order shows no obvious pattern, so there
 is no reason to suspect any violation of the independence assumption.
\end_layout

\begin_layout Section
Model Specification and Hypotheses Statement
\end_layout

\begin_layout Standard
Based on the description of the problem and the actual data, we may write
 a model for the experiment.
 For any of the test measurements, i.e., PRE1, POST2, etc., there are three
 treatment, Basal, DRTA and Strat.
 Each treatment has 22 observations.
 The model is: (for any of the tests)
\begin_inset Formula 
\[
y_{ij}=\mu+\tau_{i}+\epsilon_{ij}\,\,\begin{cases}
i=1,2,3\\
j=1,2,\ldots,22
\end{cases}
\]

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $y_{ij}$
\end_inset

 is the 
\begin_inset Formula $ij$
\end_inset

th observation, 
\begin_inset Formula $\mu$
\end_inset

 is the overall mean of all data, and 
\begin_inset Formula $\tau_{i}$
\end_inset

 is the 
\begin_inset Formula $i$
\end_inset

th treatment effect.
\end_layout

\begin_layout Standard
Having specified the model, we now write the hypotheses to be test in terms
 of the treatment effects 
\begin_inset Formula $\tau_{i}$
\end_inset

:
\begin_inset Formula 
\[
\begin{array}{ll}
H_{0}: & \tau_{1}=\tau_{2}=\tau_{3}=0\\
H_{1}: & \tau_{i}\ne0,\text{for at least one }i
\end{array}
\]

\end_inset


\end_layout

\begin_layout Standard
Thus we speak of testing that the treatment effects are zero.
\end_layout

\begin_layout Section
ANOVA
\end_layout

\begin_layout Standard
After analyzing the residuals to check the assumptions, specifying the model,
 and stating the null and alternative hypotheses, we can run ANOVA in Minitab
 now.
 We divide this part into two sections, one for Pretest and the other for
 Posttest.
\end_layout

\begin_layout Subsection
ANOVA for Pretest
\end_layout

\begin_layout Subsubsection
PRE1
\end_layout

\begin_layout LyX-Code
Source  DF      SS     MS     F      P
\end_layout

\begin_layout LyX-Code
Group    2   20.58  10.29  1.13  0.329
\end_layout

\begin_layout LyX-Code
Error   63  572.45   9.09
\end_layout

\begin_layout LyX-Code
Total   65  593.03
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

S = 3.014   R-Sq = 3.47%   R-Sq(adj) = 0.41%
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

                          Individual 95% CIs For Mean Based on
\end_layout

\begin_layout LyX-Code
                          Pooled StDev
\end_layout

\begin_layout LyX-Code
Level   N    Mean  StDev   -+---------+---------+---------+--------
\end_layout

\begin_layout LyX-Code
Basal  22  10.500  2.972                (------------*------------)
\end_layout

\begin_layout LyX-Code
DRTA   22   9.727  2.694        (------------*------------)
\end_layout

\begin_layout LyX-Code
Strat  22   9.136  3.342   (-----------*------------)
\end_layout

\begin_layout LyX-Code
                           -+---------+---------+---------+--------
\end_layout

\begin_layout LyX-Code
                          8.0       9.0      10.0      11.0
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Pooled StDev = 3.014
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Grouping Information Using Tukey Method
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   N    Mean  Grouping
\end_layout

\begin_layout LyX-Code
Basal  22  10.500  A
\end_layout

\begin_layout LyX-Code
DRTA   22   9.727  A
\end_layout

\begin_layout LyX-Code
Strat  22   9.136  A
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Means that do not share a letter are significantly different.
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Tukey 95% Simultaneous Confidence Intervals
\end_layout

\begin_layout LyX-Code
All Pairwise Comparisons among Levels of Group
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Individual confidence level = 98.05%
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group = Basal subtracted from:
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   Lower  Center  Upper   --+---------+---------+---------+-------
\end_layout

\begin_layout LyX-Code
DRTA   -2.951  -0.773  1.406       (------------*-------------)
\end_layout

\begin_layout LyX-Code
Strat  -3.542  -1.364  0.815   (------------*-------------)
\end_layout

\begin_layout LyX-Code
                               --+---------+---------+---------+-------
\end_layout

\begin_layout LyX-Code
                              -3.2      -1.6      -0.0       1.6
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group = DRTA subtracted from:
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   Lower  Center  Upper   --+---------+---------+---------+-------
\end_layout

\begin_layout LyX-Code
Strat  -2.770  -0.591  1.588        (------------*-------------)
\end_layout

\begin_layout LyX-Code
                               --+---------+---------+---------+-------
\end_layout

\begin_layout LyX-Code
                              -3.2      -1.6      -0.0       1.6
\end_layout

\begin_layout Standard
From the above results, the null hypothesis cannot be rejected at 95% confidence
 level because the p-value is 
\begin_inset Formula $0.329>0.05$
\end_inset

.
 So there is no significant difference between the three groups under PRE1.
\end_layout

\begin_layout Standard
We can draw the same conclusion from the Tukey method, which shows that
 the mean scores of the three groups are not different at 95% confidence
 level.
\end_layout

\begin_layout Subsubsection
PRE2
\end_layout

\begin_layout LyX-Code
Source  DF      SS    MS     F      P
\end_layout

\begin_layout LyX-Code
Group    2    1.12  0.56  0.11  0.895
\end_layout

\begin_layout LyX-Code
Error   63  317.14  5.03
\end_layout

\begin_layout LyX-Code
Total   65  318.26
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

S = 2.244   R-Sq = 0.35%   R-Sq(adj) = 0.00%
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

                         Individual 95% CIs For Mean Based on
\end_layout

\begin_layout LyX-Code
                         Pooled StDev
\end_layout

\begin_layout LyX-Code
Level   N   Mean  StDev  ---+---------+---------+---------+------
\end_layout

\begin_layout LyX-Code
Basal  22  5.273  2.763       (---------------*---------------)
\end_layout

\begin_layout LyX-Code
DRTA   22  5.091  1.998    (---------------*---------------)
\end_layout

\begin_layout LyX-Code
Strat  22  4.955  1.864  (---------------*---------------)
\end_layout

\begin_layout LyX-Code
                         ---+---------+---------+---------+------
\end_layout

\begin_layout LyX-Code
                          4.20      4.80      5.40      6.00
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Pooled StDev = 2.244
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Grouping Information Using Tukey Method
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   N   Mean  Grouping
\end_layout

\begin_layout LyX-Code
Basal  22  5.273  A
\end_layout

\begin_layout LyX-Code
DRTA   22  5.091  A
\end_layout

\begin_layout LyX-Code
Strat  22  4.955  A
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Means that do not share a letter are significantly different.
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Tukey 95% Simultaneous Confidence Intervals
\end_layout

\begin_layout LyX-Code
All Pairwise Comparisons among Levels of Group
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Individual confidence level = 98.05%
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group = Basal subtracted from:
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   Lower  Center  Upper  ---------+---------+---------+---------+
\end_layout

\begin_layout LyX-Code
DRTA   -1.803  -0.182  1.440   (---------------*---------------)
\end_layout

\begin_layout LyX-Code
Strat  -1.940  -0.318  1.303  (---------------*---------------)
\end_layout

\begin_layout LyX-Code
                              ---------+---------+---------+---------+
\end_layout

\begin_layout LyX-Code
                                    -1.0       0.0       1.0       2.0
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group = DRTA subtracted from:
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   Lower  Center  Upper  ---------+---------+---------+---------+
\end_layout

\begin_layout LyX-Code
Strat  -1.758  -0.136  1.485   (----------------*---------------)
\end_layout

\begin_layout LyX-Code
                              ---------+---------+---------+---------+
\end_layout

\begin_layout LyX-Code
                                    -1.0       0.0       1.0       2.0
\end_layout

\begin_layout Standard
From the above results, the null hypothesis cannot be rejected at 95% confidence
 level because the p-value is 
\begin_inset Formula $0.895\gg0.05$
\end_inset

.
 So there is no significant difference between the three groups under PRE2.
\end_layout

\begin_layout Standard
We can draw the same conclusion from the Tukey method, which shows that
 the mean scores of the three groups are not different at 95% confidence
 level.
\end_layout

\begin_layout Subsection
ANOVA for Posttest
\end_layout

\begin_layout Subsubsection
POST1
\end_layout

\begin_layout LyX-Code
Source  DF     SS    MS     F      P
\end_layout

\begin_layout LyX-Code
Group    2  108.1  54.1  5.32  0.007
\end_layout

\begin_layout LyX-Code
Error   63  640.5  10.2
\end_layout

\begin_layout LyX-Code
Total   65  748.6
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

S = 3.189   R-Sq = 14.44%   R-Sq(adj) = 11.73%
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout LyX-Code
                         Individual 95% CIs For Mean Based on
\end_layout

\begin_layout LyX-Code
                         Pooled StDev
\end_layout

\begin_layout LyX-Code
Level   N   Mean  StDev  -----+---------+---------+---------+----
\end_layout

\begin_layout LyX-Code
Basal  22  6.682  2.767  (---------*--------)
\end_layout

\begin_layout LyX-Code
DRTA   22  9.773  2.724                       (--------*--------)
\end_layout

\begin_layout LyX-Code
Strat  22  7.773  3.927          (--------*--------)
\end_layout

\begin_layout LyX-Code
                         -----+---------+---------+---------+----
\end_layout

\begin_layout LyX-Code
                            6.0       7.5       9.0      10.5
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Pooled StDev = 3.189
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Grouping Information Using Tukey Method
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   N   Mean  Grouping
\end_layout

\begin_layout LyX-Code
DRTA   22  9.773  A
\end_layout

\begin_layout LyX-Code
Strat  22  7.773  A B
\end_layout

\begin_layout LyX-Code
Basal  22  6.682    B
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Means that do not share a letter are significantly different.
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Tukey 95% Simultaneous Confidence Intervals
\end_layout

\begin_layout LyX-Code
All Pairwise Comparisons among Levels of Group
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Individual confidence level = 98.05%
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group = Basal subtracted from:
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   Lower  Center  Upper  -------+---------+---------+---------+--
\end_layout

\begin_layout LyX-Code
DRTA    0.786   3.091  5.395                      (--------*---------)
\end_layout

\begin_layout LyX-Code
Strat  -1.214   1.091  3.395              (--------*---------)
\end_layout

\begin_layout LyX-Code
                              -------+---------+---------+---------+--
\end_layout

\begin_layout LyX-Code
                                  -2.5       0.0       2.5       5.0
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group = DRTA subtracted from:
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   Lower  Center  Upper  -------+---------+---------+---------+--
\end_layout

\begin_layout LyX-Code
Strat  -4.305  -2.000  0.305  (--------*--------)
\end_layout

\begin_layout LyX-Code
                              -------+---------+---------+---------+--
\end_layout

\begin_layout LyX-Code
                                  -2.5       0.0       2.5       5.0
\end_layout

\begin_layout Standard
From the above results, the null hypothesis should be rejected at 95% confidence
 level because the p-value is 
\begin_inset Formula $0.007<0.05$
\end_inset

.
 So there is significant difference between the three groups under POST1.
\end_layout

\begin_layout Standard
We can make pairwise comparison among three groups using Tukey method.
 The results show that Basal and Strat, and DRTA and Strat, both have equal
 mean, respectively, at 95% confidence level.
 However, Basal and DRTA have different means at 95% confidence level.
 Moreover, DRTA has significant higher mean than Basal.
\end_layout

\begin_layout Subsubsection
POST2
\end_layout

\begin_layout LyX-Code
Source  DF      SS     MS     F      P
\end_layout

\begin_layout LyX-Code
Group    2   95.12  47.56  8.41  0.001
\end_layout

\begin_layout LyX-Code
Error   63  356.41   5.66
\end_layout

\begin_layout LyX-Code
Total   65  451.53
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

S = 2.379   R-Sq = 21.07%   R-Sq(adj) = 18.56%
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

                         Individual 95% CIs For Mean Based on Pooled StDev
\end_layout

\begin_layout LyX-Code
Level   N   Mean  StDev    +---------+---------+---------+---------
\end_layout

\begin_layout LyX-Code
Basal  22  5.545  2.041    (------*------)
\end_layout

\begin_layout LyX-Code
DRTA   22  6.227  2.092         (------*-----)
\end_layout

\begin_layout LyX-Code
Strat  22  8.364  2.904                       (------*------)
\end_layout

\begin_layout LyX-Code
                           +---------+---------+---------+---------
\end_layout

\begin_layout LyX-Code
                         4.5       6.0       7.5       9.0
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Pooled StDev = 2.379
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Grouping Information Using Tukey Method
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   N   Mean  Grouping
\end_layout

\begin_layout LyX-Code
Strat  22  8.364  A
\end_layout

\begin_layout LyX-Code
DRTA   22  6.227    B
\end_layout

\begin_layout LyX-Code
Basal  22  5.545    B
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Means that do not share a letter are significantly different.
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Tukey 95% Simultaneous Confidence Intervals
\end_layout

\begin_layout LyX-Code
All Pairwise Comparisons among Levels of Group
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Individual confidence level = 98.05%
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group = Basal subtracted from:
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   Lower  Center  Upper  -----+---------+---------+---------+----
\end_layout

\begin_layout LyX-Code
DRTA   -1.037   0.682  2.401             (------*------)
\end_layout

\begin_layout LyX-Code
Strat   1.099   2.818  4.537                     (------*------)
\end_layout

\begin_layout LyX-Code
                              -----+---------+---------+---------+----
\end_layout

\begin_layout LyX-Code
                                -2.5       0.0       2.5       5.0
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group = DRTA subtracted from:
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group  Lower  Center  Upper  -----+---------+---------+---------+----
\end_layout

\begin_layout LyX-Code
Strat  0.417   2.136  3.855                   (------*-----)
\end_layout

\begin_layout LyX-Code
                             -----+---------+---------+---------+----
\end_layout

\begin_layout LyX-Code
                               -2.5       0.0       2.5       5.0
\end_layout

\begin_layout Standard
From the above results, the null hypothesis should be rejected at 95% confidence
 level because the p-value is 
\begin_inset Formula $0.001<0.05$
\end_inset

.
 So there is significant difference between the three groups under POST2.
\end_layout

\begin_layout Standard
We can make pairwise comparison among three groups using Tukey method.
 The results show that Basal and DRTA have equal mean at 95% confidence
 level, while Strat's mean is different from Basal's and DRTA's at 95% confidenc
e level.
 Furthermore, the mean of Strat is significant higher than that of Basal
 and DRTA.
\end_layout

\begin_layout Subsubsection
POST3
\end_layout

\begin_layout LyX-Code
Source  DF      SS     MS     F      P
\end_layout

\begin_layout LyX-Code
Group    2   357.3  178.7  4.48  0.015
\end_layout

\begin_layout LyX-Code
Error   63  2511.7   39.9
\end_layout

\begin_layout LyX-Code
Total   65  2869.0
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

S = 6.314   R-Sq = 12.45%   R-Sq(adj) = 9.67%
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

                          Individual 95% CIs For Mean Based on
\end_layout

\begin_layout LyX-Code
                          Pooled StDev
\end_layout

\begin_layout LyX-Code
Level   N    Mean  StDev   --+---------+---------+---------+-------
\end_layout

\begin_layout LyX-Code
Basal  22  41.045  5.636   (--------*--------)
\end_layout

\begin_layout LyX-Code
DRTA   22  46.727  7.388                      (--------*--------)
\end_layout

\begin_layout LyX-Code
Strat  22  44.273  5.767              (--------*--------)
\end_layout

\begin_layout LyX-Code
                           --+---------+---------+---------+-------
\end_layout

\begin_layout LyX-Code
                          39.0      42.0      45.0      48.0
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Pooled StDev = 6.314
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Grouping Information Using Tukey Method
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   N    Mean  Grouping
\end_layout

\begin_layout LyX-Code
DRTA   22  46.727  A
\end_layout

\begin_layout LyX-Code
Strat  22  44.273  A B
\end_layout

\begin_layout LyX-Code
Basal  22  41.045    B
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Means that do not share a letter are significantly different.
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Tukey 95% Simultaneous Confidence Intervals
\end_layout

\begin_layout LyX-Code
All Pairwise Comparisons among Levels of Group
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Individual confidence level = 98.05%
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group = Basal subtracted from:
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   Lower  Center   Upper  ----+---------+---------+---------+-----
\end_layout

\begin_layout LyX-Code
DRTA    1.118   5.682  10.245                  (--------*--------)
\end_layout

\begin_layout LyX-Code
Strat  -1.336   3.227   7.791             (--------*---------)
\end_layout

\begin_layout LyX-Code
                               ----+---------+---------+---------+-----
\end_layout

\begin_layout LyX-Code
                                -5.0       0.0       5.0      10.0
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group = DRTA subtracted from:
\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

Group   Lower  Center  Upper  ----+---------+---------+---------+-----
\end_layout

\begin_layout LyX-Code
Strat  -7.018  -2.455  2.109  (--------*--------)
\end_layout

\begin_layout LyX-Code
                              ----+---------+---------+---------+-----
\end_layout

\begin_layout LyX-Code
                               -5.0       0.0       5.0      10.0
\end_layout

\begin_layout Standard
From the above results, the null hypothesis should be rejected at 95% confidence
 level because the p-value is 
\begin_inset Formula $0.015<0.05$
\end_inset

.
 So there is significant difference between the three groups under POST3.
\end_layout

\begin_layout Standard
We can make pairwise comparison among three groups using Tukey method.
 The results show that Basal and Strat, and DRTA and Strat, both have equal
 mean, respectively, at 95% confidence level.
 However, Basal and DRTA have different means at 95% confidence level.
 Moreover, DRTA has significant higher mean than Basal.
\end_layout

\begin_layout Section
Conclusion
\end_layout

\begin_layout Standard
From the above analysis we find that the ANOVA confirms our observation
 of the data and the Box plots, we make the following conclusion based on
 the preliminary observation and the ANOVA results:
\end_layout

\begin_layout Itemize
There is no significant difference in reading comprehension ability, before
 the experiment, among three groups based on both PRE1 and PRE2 tests.
\end_layout

\begin_layout Itemize
There is significant difference in reading comprehension ability after the
 experiment among three groups, which is demonstrated by all three posttests.
 Specifically, DRTA has a higher mean score than the other two groups based
 on POST1 and POST3 tests, while Strat has better performance concerning
 POST2 test.
\end_layout

\begin_layout Itemize
Concerning the first reading comprehension measure, the scores of the three
 groups have no obvious difference before and after the experiment.
 While for the second reading comprehension measure, the scores increase
 a little after the experiment.
\end_layout

\begin_layout Itemize
The first reading comprehension measure tends to give a little higher score
 than the second one, while the third measure gives much huger score than
 the first and second one.
\end_layout

\end_body
\end_document
