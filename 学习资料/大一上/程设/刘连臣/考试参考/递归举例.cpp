/*yqy 2016.1*/
/*递归举例*/
/*大致是把做过的一些递归题放在一起*/
/*收录了
1、分书问题
2、字符输出
3、有序字符串的拼接
4、星号匹配*/ 

#include<string.h>
#include<stdio.h>

/*分书 第5次作业第12题*/ 
void eg1()
{
	void assign(int n, int like[][5], int sol[], int choosed[],int solNum[]);
	int like[5][5] = {{0,0,1,1,0}, {1,1,0,0,1}, {0,1,1,0,1}, {0,0,0,1,0},
					{0,1,0,0,1}};
	int sol[5]={0}, choosed[5]={0}, solNum[1]={0};
	assign(0, like, sol, choosed, solNum);
}
void assign(int n, int like[][5], int sol[], int choosed[], int solNum[])
{
	int i;
	if(n == 5) //递归到第5层，输出一个可行解
	{
		printf("第%d组解：\n", ++solNum[0]);
		printf(" A B C D E\n书 ");
		for(i=0; i<5; i++)
		printf("%d ", sol[i]);
		printf("\n");
	}
	else
 	{ 
		for(i=0; i<5; i++) 
		//第i本书没有被选，且第1个人对其有喜好 
		if(choosed[i]!=1 && like[n][i] == 1) 
		{ 
			sol[n] = i; choosed[i] = 1; 
			//选书后进入下层递归 
			assign(n+1, like, sol, choosed, solNum); 
			//递归结束后重置状态，供其它递归使用 
			choosed[i] = 0; 
		}
	}		
}

/*第5次作业第九题 
例如，输入aybdx，则递归程序输出XaDyBbYdAx*/
void eg2()
{
	void strIntersect(char str[], int length, int currentDigit);
	char str[20]; 
	printf("输入一个小写英文字符串（长度大于4小于20）: \n"); 
	gets(str); 
	printf("递归输出结果字符串: \n"); 
	strIntersect(str, strlen(str), 0); 
	printf("\n");
}
//三个参数分别代表该字符串，字符串的长度，当前处理位的下标 
void strIntersect(char str[], int length, int currentDigit) 
{ 
	if(currentDigit<length) //没有越界的情况下继续递归 
	{ 
		printf("%c%c", str[length-currentDigit-1]-32, str[currentDigit]); 
		strIntersect(str, length, currentDigit+1); 
	} 
}

/*2015.1期末考试最后一题
	利用递归，合并有序字符串*/
void eg3()
{
	char *combine(char *str1,char *str2,int n1,int n2); 
	char str1[20],str2[20],*str3,q[10];
	while(1)
	{
		printf("程序是否继续执行？按quit或exit程序退出：\n");
		gets(q);
		if (!strcmp(q,"exit") || !strcmp(q,"quit"))
			break;
		fflush(stdin);
		printf("请输入字符串str1：");gets(str1);
		printf("请输入字符串str2：");gets(str2);
		str3=combine(str1,str2,0,0);
		printf("合并后的字符串：%s\n\n",str3);
	}
}
char *combine(char *str1,char *str2,int n1,int n2)
{
	static char str3[40];//采用静态可以少传递一个变量 
	static int z;
	if (n1==0 && n2==0)//重置静态变量 
	{
		z=0;
		memset(str3,0,sizeof(str3));
	}
	//printf("str3=%s n1=%d n2=%d\n",str3,n1,n2);
	if(str1[n1]=='\0'|| str2[n2]=='\0')
	{
		if (str1[n1]=='\0' && str2[n2]=='\0')
		{
			str3[z]='\0';		
			return str3;
		}		
		if (str1[n1]=='\0')
		{
			str3[z++]=str2[n2];
			return combine(str1,str2,n1,n2+1);
		}
		if (str2[n2]=='\0')
		{
			str3[z++]=str1[n1];
			return combine(str1,str2,n1+1,n2);
		}
	}
	if (str1[n1]<str2[n2])
	{
		str3[z++]=str1[n1];
		return combine(str1,str2,n1+1,n2);
	}
	else
	{
		str3[z++]=str2[n2];
		return combine(str1,str2,n1,n2+1);
	}
}

/*2013最后一题 
	星号匹配问题
	ps. 有点小bug，不去管它了,
	abcdcde VS a*cde 在我算法中不匹配*/
void eg4()
{
	int PatternMatch(char *s1,char *s2,int n1,int n2);
	char str1[40],str2[40],q[]="quit";
	int flag;
	while(1)
	{
		printf("请输入字符串str1：");gets(str1);
		printf("请输入字符串str1：");gets(str2);
		if (strcmp(str1,q)==0) break;
		flag=PatternMatch(str1,str2,0,0);
		if (flag) printf("匹配成功！\n");
		else printf("匹配失败！\n"); 
	}
}
int PatternMatch(char *s1,char *s2,int n1,int n2)
{
	int len1=strlen(s1),len2=strlen(s2);
	
	if (n1==len1 && n2==len2) return 1;
	//printf("n1=%d,n2=%d\n",n1,n2);
	if (s2[n2]=='*')
	{
		if(s2[n2+1]==s1[n1])
			return PatternMatch(s1,s2,n1,n2+1);
		else
			return PatternMatch(s1,s2,n1+1,n2);
	}
	else if (s2[n2]==s1[n1])
		return PatternMatch(s1,s2,n1+1,n2+1);
	else
		return 0;
} 
int main()
{
	//eg1();
	//eg2();
	//eg3();
	//eg4();
	return 0;
}
