/*yqy 2016.1*/
/*字符串自编函数
	发现灵活使用strcpy实在是件逆天的事情
	收录：
	1、Delete函数（在字符串s中，从s[loc]起（包括）删除n个字符） 
	2、find函数（为了记住重复出现的位置和个数，又不想用结构体，只好用下标了orz）
	3、insert函数（i love apples+round +3=i love round apples）
	4、compact函数（作业题，1,3,6+空格保留，其余删除） 
	5、getWords函数（对带空格的字符串进行单词分离） 
字符串考法比较灵活，想来想去也没什么特别好的函数，到时灵活运用吧orz*/

#include<string.h>
#include<stdio.h>
#include<malloc.h>

void Delete(char *s,int loc,int n)//loc 删除的起始地点，n 删除的个数 
{
	char temp[20];
	/*way1: strncpy(temp1,s,loc);temp1[loc]='\0';//strncpy需要补充\0 
			strcpy(temp2,s+loc+n);
			strcat(temp1,temp2);
			strcpy(s,temp1);*/
	strcpy(temp,s+loc+n);//way2;
	strcpy(s+loc,temp);
}
int *find(char *s)//找到一个字符串中连续出现三个及以上字符的位置和数量，如123332，a[2]=3,其他都是-1; 
{
	int i,j,*a,count,loc,len=strlen(s);
	a=(int *)malloc(len*sizeof(int));
	char t;
	for(i=0;i<len;i++)
		a[i]=-1;
	for(i=0;i<len;)
	{
		if (s[i]==s[i+1] && s[i+1]==s[i+2])
		{
			count=3;t=s[i];loc=i;i=i+3;
			while(s[i]==t)
			{
				count++;i++;
			}
			a[loc]=count;
		}
		else
			i++;
	}
	return a;
}
void insert(char *s,char *t,int n)//2014.1第一题， 
{
	int i,j,len1=strlen(s),len2=strlen(t);
	char temp[100];
	for(i=n;i<len1;i++)
		if (s[i]==' ')
			break;
	if (i!=len1)
	{
		strcpy(temp,s+i+1);
		strcpy(s+i+1,t);
		strcpy(s+i+1+len2,temp);
	}
	else
		strcpy(s+i,t);
}
void compact(char *str)//作业题 
{
	int a[100]={0}, len, i, j;
	len = strlen(str);
	for(i = 0; i < len; i++)
	{
		int occ = 0; //统计每个字符是第几次出现，并存入数组a
		for(j = 0; j <= i; j++)
		{
			if(*(str+j) == *(str+i))
				occ++;
		}
		a[i] = occ;
	}
	int k = 0;
	char newstr[100];
	for(i = 0; i < len; i++)
	{
		if(a[i] == 1 || a[i] == 3 || a[i] == 6 || *(str+i) == ' ')
		{ //如果出现次数是1,3,6或空格，则保留
			*(newstr+k) = *(str+i);
			k++;
		}
	}
	*(newstr+k) = '\0'; //注意给字符串添加结尾福
	strcpy(str, newstr);
}

//定义分词函数，对str 进行分词并返回，单词个数通过n 返回
char** getWords(char *str, int *n)
{
	char **keys = (char **)malloc(sizeof(char*) * 100); //假设单词个数不超过100
	int i = 0, j, k=0;
	while(str[i] != '\0')
	{
		if(str[i] != ' ')
		{
			*(keys+k) = (char *)malloc(sizeof(char) * 40); //假设每个单词长度不超40
			j = 0;
			while(str[i] != ' ' && str[i] != '\0')
			{
				keys[k][j] = str[i];
				j++;
				i++;
			}
			keys[k][j] = '\0';
			k++;
		}
		else
			i++;
	}
	*n = k;
	return keys;
}

int main()
{
	char str[]="123456789",str1[]="quit";
	char **keys;
	char test[]="i love      DA54 ! ! !";
	int n;//test 个数 
	keys=getWords(test,&n);
	for(int i=0;i<n;i++)
		printf("the word %d is %s\n",i,keys[i]);
	//printf("%d",Strlen(str));
	//Strcpy(str,str1);
	//printf("%s",str);
	//printf("the old string is %s\n",str);
	//Delete(str,2,3);
	//printf("the new string is %s\n",str);
	return 0;
} 
